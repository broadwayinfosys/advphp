<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-13 07:29:12
 * @Organization: Knockout System Pvt. Ltd.
 */
include 'inc/header.php';

if(!isset($_SESSION['username']) || !isset($_SESSION['role']) || $_SESSION['role']=="" ){
	$_SESSION['error'] = 'You are not logged in.';
	header('location: index.php');
	exit;
}
if($_SESSION['role'] == 'admin'){
?>
	<ul>
		<li><a href="logout.php" title="Logout">Logout</a></li>
		<li><a href="user.php">Users List</a></li>
		<li><a href="post.php">Posts List</a></li>

	</ul>
<?php
} else if($_SESSION['role'] == 'user'){
?>
	<ul>
		<li><a href="post.php">Posts List</a></li>
	</ul>

<?php
} else if($_SESSION['role'] == 'guest'){
?>
	<ul>
		<li><a href="post-detail.php">Posts</a></li>
	</ul>

<?php
}

if($_SESSION['role'] != "admin"){
	$_SESSION['warning'] = "Access Denied";
	header('location: profile.php');
	exit;
}


//Form Processing 
if(isset($_POST['submit']) && $_POST['submit']!=""){
	
	$full_name = sanitization($_POST['full_name']);
	$email_address = sanitization($_POST['email_address']);
	$address = sanitization($_POST['address']);
	$phone_number = sanitization($_POST['phone_number']);
	$gender = sanitization($_POST['gender']);
	$description = htmlentities(sanitization($_POST['description']));
	//htmlentities() decoding function html_entity_decode()

	$age = (int)sanitization($_POST['age']);
	$status = (int)sanitization($_POST['status']);


	if(isset($_POST['id']) && $_POST['id']!=""){
		$id = (int)sanitization($_POST['id']);
		$first = "UPDATE student_info SET ";
		$last = " WHERE id = ".$id;
	} else {
		$first = "INSERT INTO student_info SET added_date = now(), ";
		$last = "";
	}
	$sql = $first." name = '{$full_name}',
			email_address = '{$email_address}',
			address = '{$address}',
			phone_number = '{$phone_number}',
			description = '{$description}',
			gender = '{$gender}',
			age = $age,
			status = $status " . $last;
	/*echo $sql;
	exit;*/
	$query = $conn->query($sql);
	if($query){
		$_SESSION['success'] = "Student Information Updated successfully";

		header('location: user.php');
		exit;
	} else {
		$_SESSION['error'] = "There was problem while adding the student information.";

		header('location: user.php');
		exit;
	}
}


//Action checking for edit
if(isset($_GET['id']) && isset($_GET['action']) && $_GET['id'] != ""){
	if($_GET['action'] == substr(md5('edit-user-'.$_GET['id']), 0,10)){
		$id = sanitization($_GET['id']);
		$student = getStudentInfoById($id);
	} else {
		$_SESSION['warning'] = "The Id you requested does not exists.";
		header('location: user.php');
	}
}
?>
	<div class="container">
		<h4>Student Add</h4>
		<form class="form-horizontal" name="student-add" method="post" action="user-add.php">
			<div class="form-group">
				<label>Name *:</label>
				<input type="text" class="form-control" name="full_name" value="<?php echo (isset($student) && $student['name']!='')? $student['name']:'';?>" required id="full_name" />
			</div>

			<div class="form-group">
				<label>Email *:</label>
				<input type="email" class="form-control" name="email_address" required id="email_address" value="<?php echo (isset($student) && $student['email_address']!='')? $student['email_address']:'';?>"/>
			</div>

			<div class="form-group">
				<label>Address *:</label>
				<input type="text" class="form-control" name="address" required id="address" value="<?php echo (isset($student) && $student['address']!='')? $student['address']:'';?>" />
			</div>

			<div class="form-group">
				<label>Phone Number *:</label>
				<input type="tel" class="form-control" name="phone_number" required id="phone_number" value="<?php echo (isset($student) && $student['phone_number']!='')? $student['phone_number']:'';?>"/>
			</div>

			<div class="form-group">
				<label>Gender *:</label>
				<input type="radio" name="gender" value="male" <?php echo (isset($student) && $student['gender'] == 'male') ? 'checked' : '';?> checked /> Male &nbsp;&nbsp;
				<input type="radio" name="gender" value="female" <?php echo (isset($student) && $student['gender'] == 'female') ? 'checked' : '';?> /> Female &nbsp;&nbsp;
				<input type="radio" name="gender" value="other" <?php echo (isset($student) && $student['gender'] == 'other') ? 'checked' : '';?> /> Other
			</div>
			
			<div class="form-group">
				<label>Description *:</label>
				<textarea name="description" required id="description" class="form-control" style="resize: vertical;" rows="3"><?php echo (isset($student) && $student['description'] != '') ? html_entity_decode($student['description']) : '';?></textarea>
			</div>
			
			<div class="form-group">
				<label>Age *:</label>
				<input type="number" min="1" step="1" max="100" name="age" id="age" required class="form-control" value="<?php echo (isset($student) && $student['age'] != '') ? (int)$student['age'] : '';?>" />
			</div>

			<div class="form-group">
				<label>Status *:</label>
				<select name="status" required class="form-control col-md-4">
					<option value="1" <?php echo (isset($student) && $student['status'] == 1) ? 'selected' : '';?>>Active</option>
					<option value="0" <?php echo (isset($student) && $student['status'] == 0) ? 'selected' : '';?>>Inactive</option>
				</select>
			</div>

			<div class="form-group">
				<input type="hidden" name="id" value="<?php echo (isset($_GET['id']) && $_GET['id']!= '') ? $_GET['id'] : '';?>" />
				<input type="submit" name="submit" class="btn btn-primary" value="<?php echo (isset($_GET['id']) && $_GET['id']!= '') ? 'Update' : 'Submit';?>" />
			</div>
		</form>
	</div>

<?php include 'inc/footer.php'; ?>