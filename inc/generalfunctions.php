<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-28 07:41:48
 * @Organization: Knockout System Pvt. Ltd.
 */
function debugger($array, $isDie=false){
	echo "<pre>";
	print_r($array);
	echo "</pre>";
	if($isDie){
		exit;
	}
}

//Sanitization function
function sanitization($var){
	global $conn;
	return $conn->real_escape_string($var);
}

//Fetching all student Information from the database
function getAllStudents($isDie=false){	//To print SQL 
	global $conn;
	$sql = "SELECT * FROM student_info ORDER BY id DESC";
	if($isDie){	//TO test What sql is querying in the db
		echo $sql;
		exit;
	}
	$query = $conn->query($sql);
	if($query->num_rows <= 0){
		return false;
	} else {
		$data = array();	//TO store data
		while($row = $query->fetch_assoc()){
			$data[] = $row;	//Table Row stored or pushed into $data variable
		}
		return $data;
	}


}

function deleteData($table, $field, $value, $isDie=false){
	global $conn;
	$sql = "DELETE FROM ".$table." WHERE ".$field." = '{$value}' ";
	if($isDie){
		echo $sql;
		exit;
	} else {
		$query = $conn->query($sql);
		if($query){
			return true;
		} else {
			return false;
		}
	}
}

//Select user information by id
function getStudentInfoById($id, $isDie=false){
	global $conn;
	$sql = "SELECT * FROM student_info WHERE id = ".$id;
	if($isDie){
		echo $sql;
		exit;
	} else {
		$query = $conn->query($sql);
		if($query->num_rows <=0 ){
			return false;
		} else {
			$data = $query->fetch_assoc();
			return $data;
		}
	}
}
?>
