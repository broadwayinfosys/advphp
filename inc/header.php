<?php session_start();?>
<?php include 'inc/config.php'; ?>
<?php include 'inc/generalfunctions.php';?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $pageName;?></title>
	<link rel="shortcut icon" href="upload/06-Contact.jpg" />
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css" />

	<script type="text/javascript" src="assets/js/jquery-3.1.1.min.js"></script>
	<style type="text/css">
		ul{
			list-style: none;
		}
		li {
		    float: right;
		    margin-right: 10px;
		    padding: 10px 20px 10px 20px;
		    border: 1px solid #000;
		    border-radius: 10px;
		    background: #ccc;
		    cursor: pointer;
		}
	</style>
</head>
<body>