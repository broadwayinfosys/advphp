<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-26 08:03:30
 * @Organization: Knockout System Pvt. Ltd.
 */

define('DB_HOST','localhost');
define('DB_USER','root');
define('DB_PW','');
define('DB_NAME','advanced_php');

$conn = mysqli_connect(DB_HOST,DB_USER,DB_PW) or die(mysqli_error());

$db = mysqli_select_db($conn,DB_NAME) or die(mysqli_error());
?>