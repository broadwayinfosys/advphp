<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-12 07:33:35
 * @Organization: Knockout System Pvt. Ltd.
 */
	session_start();
	include 'inc/functions.php';

	$user = array(
				array(
					"username" => "admin",
					"password" => sha1('admin'),
					"role" => "admin" ),
				array(
					"username" => "user",
					"password" => sha1('user'),
					"role" => "user" ),
				array(
					"username" => "guest",
					"password" => sha1('guest'),
					"role" => "guest" ),
				);

	if(isset($_POST['submit']) && $_POST['submit'] == "Login"){
		$username = $_POST['username'];
		$password = $_POST['password'];
		$enc_pwd = sha1($password);
		$remember_me = (isset($_POST['remember_me'])) ? $_POST['remember_me'] : 0 ;

		//debugger($_POST,true);

		foreach ($user as $key => $value) {
			if($value['username'] == $username && $value['password'] == $enc_pwd){
				//Status Message set
				$_SESSION['success'] = "You are successfully logged in as ".$value['role'];

				$_SESSION['username'] = $value['username'];
				$_SESSION['role'] = $value['role'];
				if($remember_me == 1){

					$_COOKIE['username'] = $value['username'];
					$_COOKIE['role'] = $value['role'];
					//implode
					/*$cookieValue = implode(",", $_COOKIE);
					echo $cookieValue;
					
					$cookieValue = "username=".$value['username'].",role=".$value['role'];

					echo "<br/>";
					*/
					$cookieData = serialize($_COOKIE);
					/*echo $cookieData;
					echo "<br/>";

					$unserialized_data = unserialize($cookieData);
					debugger($unserialized_data,true); */

					setcookie("user",$cookieData,time()+84600);

				}
				
				//header Loop
				header('location: dashboard.php');
				exit;
			}			
		}
		$_SESSION['error'] = "Username or password does not match";
		header('location: index.php');
		exit;
	} else {
		
		$_SESSION['warning'] = "Illegal entry";
		header('location: index.php');
		exit;
	}

?>
