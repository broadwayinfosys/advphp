<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-13 07:29:12
 * @Organization: Knockout System Pvt. Ltd.
 */
include 'inc/header.php';


if(!isset($_SESSION['username']) || !isset($_SESSION['role']) || $_SESSION['role']=="" ){
	$_SESSION['error'] = 'You are not logged in.';
	header('location: index.php');
	exit;
}
if($_SESSION['role'] == 'admin'){
?>
	<ul>
		<li><a href="logout.php" title="Logout">Logout</a></li>
		<li><a href="user.php">Users List</a></li>
		<li><a href="post.php">Posts List</a></li>

	</ul>
<?php
} else if($_SESSION['role'] == 'user'){
?>
	<ul>
		<li><a href="post.php">Posts List</a></li>
	</ul>

<?php
} else if($_SESSION['role'] == 'guest'){
?>
	<ul>
		<li><a href="post-detail.php">Posts</a></li>
	</ul>

<?php
}

if($_SESSION['role'] != "admin"){
	$_SESSION['warning'] = "Access Denied";
	header('location: profile.php');
	exit;
}
?>
<div class="container">
	<div class="row">
	<?php 
		include 'inc/notifications.php';
	?>
	
	</div>
</div>

<?php 
include 'inc/footer.php';
?>