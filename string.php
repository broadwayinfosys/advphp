<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-20 08:02:05
 * @Organization: Knockout System Pvt. Ltd.
 */
include 'inc/functions.php';

$string = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat quasi qui veniam debitis blanditiis suscipit harum doloremque, molestiae illum odio. Officiis praesentium culpa adipisci qui nisi. Hic vero dolore, magnam.";

$string1 = "Hello %s %s %u";

/*
	%s = string
	%b = binary
	%u = number
	%c = ASCII Value
	%d = signed decimal number
	%e = scientific notation using lowercase (1.2e+2)
	%E = scientific notation using UpperCase (1.2E+2)
	
*/


$string2 = "World";

//echo $string;

echo "<br/>";

//echo $strin1.$string2;

$txt = sprintf($string1,"advanced PHP ","class", 5);

// printf($string1,"advanced PHP ","class", 5);

// echo $txt;

$string = "This is advanced php class.";

//Breaking or splitting a string with some delimeter
$str_array = explode(" ", $string);

//Splitting string with a certain length
$splitted_string = str_split($string,5);

//No. of characters in a string
$count = strlen($string);


//Joining string from array
$array = array("This","is","php","class.");
$imploded_str = implode(" ", $array);

$joined_str = join(" ",$array);


//Comparing String
$str1 = "This is php class";
$str2 = "This is php Class";
$str3 = "this is not php class";
$str4 = " THIS IS NOT  PHP CLASS ";

/*$comp = strcmp($str1, $str2);
echo $comp."<br/>";

$comp = strcmp($str2, $str3);
echo $comp."<br/>";

$comp = strcmp($str3, $str2);
echo $comp."<br/>";


echo ucfirst($str3); // First Character of string converted to Capital letter

echo "<br/>";

echo ucwords($str3); //Every first character of words converts to uppercase;

echo "<br/>";


echo strtoupper($str1); //Converts string to upper case

echo "<br/>";

echo strtolower($str4); //Converts string to lower case

echo "<br/>";

echo str_word_count($str4); //Returns no. of words in a string.

$str4 = trim($str4); //Removes the white space from the string at front as well as at backside

echo "<br />".$str4."<br/>";

*/
//String pattern match
$string = "This is php class. php is a serverside scripting language. php is very easy course to learn.";
$pattern = "i";

$pattern_match = strpos($string, $pattern); //First occurance of the character or pattern or needle
$pattern_match_last = strripos($string, $pattern); //Last occurance of the pattern.


//String Replacement
$replacement_string = "PHP";
$replaced_string = str_replace("php", "PHP", $string);

//echo $replaced_string;

//String Substring
$string = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum maxime Lorem dignissimos, temporibus quidem debitis, illo vitae Lorem doloremque suscipit quas, sint quia quae.Lorem Lorem Lorem Similique dolor, beatae. Debitis maxime est, voluptatem sapiente.";

$sub_string = substr($string, -9, 9);
echo $sub_string;
echo "<br/>";
echo $string;
?>
