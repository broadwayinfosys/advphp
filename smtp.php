<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-22 07:22:52
 * @Organization: Knockout System Pvt. Ltd.
 */


//SMTP server and Port Config
/*Only for localhost*/
/*ini_set('SMTP',"smtp.ntc.net.np");
ini_set('smtp_port',25);
*/
$to = "sandesh.bhattarai79@gmail.com";
$from = "sandesh.bhattarai79@gmail.com";

$subject = "This is test Email";

$message = "This is the body of the test message. Lorem ipsum dolor sit amet, consectetur adipisicing elit. ";
$message .= "Est saepe voluptas minima sed quaerat nulla ex nemo officiis eveniet suscipit! Optio consequatur quos molestiae ipsam hic quis cupiditate ";
$message .= "atque expedita.  <b> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim aspernatur atque possimus, ipsam tenetur, eligendi vero, eos nesciunt accusantium culpa iusto! </b>";


$headers = "";
$headers .= "MIME-Version: 1.0 \r\n";
$headers .= "Content-type: text/html; charset=iso-8859-1 \r\n";
$headers .= "X-Priority: 1 \r\n";
$headers .= "From: ".$from."\r\n";

$mail = mail($to, $subject, $message, $headers);
if($mail){
	echo "Mail sent successfully";
} else {
	echo "There was problem while sending email.";
}


?>
