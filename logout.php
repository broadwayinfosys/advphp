<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-13 07:38:26
 * @Organization: Knockout System Pvt. Ltd.
 */
	session_start();
	session_destroy();
	setcookie('user','',-360);
	header('location: index.php');
?>
