<?php $pageName = "Login Page"; ?>
<?php include 'inc/header.php'; ?>
<?php
	if(isset($_SESSION['username']) && $_SESSION['role'] != ""){
		header('location: dashboard.php');
		exit;
	} else if(isset($_COOKIE['user']) && $_COOKIE['user']!=""){
		$cookie_data = unserialize($_COOKIE['user']);
		if(isset($cookie_data['username']) && $cookie_data['username'] !="" && isset($cookie_data['role']) && $cookie_data['role']!=""){
			$_SESSION['username'] = $cookie_data['username'];
			$_SESSION['role'] = $cookie_data['role'];
			$_SESSION['success'] = "Welcome back ".$_SESSION['username'];
			
			header('location: dashboard.php');
			exit;
		}
	}
	 else {
		// $_SESSION['error'] = "You are not logged in.";
	}
?>
<div class="container">
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4" style="margin-top: 10%">
			<h4>Login</h4>
			
			<?php include 'inc/notifications.php'; ?>

			<form class="form-horizontal" name="login-form" method="post" action="login.php">
					<div class="form-group">
						<label>User Name:</label>
						<input type="text" name="username" class="form-control" id="username" required />
					</div>

					<div class="form-group">
						<label>Password:</label>
						<input type="password" name="password" class="form-control" id="password" required />
					</div>
					<div class="form-group">
						<input type="checkbox" name="remember_me" value="1" />&nbsp;Remember Me
					</div>
					<div class="form-group">
						<input type="submit" name="submit" class="btn btn-primary" id="submit" value="Login" />
					</div>
					
			</form>
		</div>
		<div class="col-md-4"></div>
	</div>
</div>

<?php include('inc/footer.php'); ?>