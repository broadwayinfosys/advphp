<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-03 07:55:25
 * @Organization: Knockout System Pvt. Ltd.
 */
/*
function add($x,$y = 5){
	return ($x+$y);
}*/

/*echo "<br/>";
echo add(3,4); // 7
echo "<br/>";

echo add(5); //10
echo "<br/>";*/

/*date_timezone_set($date,new DateTimeZone('Asia/Kathmandu'));
echo date('Y-m-d H:i:s A');
*/

/*function volume($length, $breadth, $height = 0){
	if($height > 0){
		echo "The volume is: " . ($length * $breadth * $height);
	} else {
		echo "The area is: " . ($length * $breadth);
	}

}
*/
/*volume(7,8,9);
echo "<br />";

volume(3,4);
echo "<br />";*/

/*Arguments by Value and Arguments by reference*/
/*$result = 6;
function argument_by_value($var){
	$var += 1;
	return $var; 
}

echo $var;

//echo argument_by_value(7); //8

function argument_by_reference(&$var){
	$var += 1;
	return $var; 
}
*/
// echo "<br />";
// echo argument_by_reference($result); //8

// echo "<br />";
//  $result1 = argument_by_value($result);

//  echo "Argument by value example. Argument passed was ".$result." After function call value is: ".$result1;


// echo "<br />";
// argument_by_reference($result);

// echo "Argument by reference example. Argument passed was 6 After function call value is: ".$result;

/*Date and time*/
/*$date = date('Y-m-d');
$date1 = date('y-M-D');
$timestamp = strtotime(date('Y-m-d'));
$yesterday = date('Y-m-d D',strtotime(date('Y-m-d')." +30 days"));

echo $date."<br />";
echo $date1."<br />";
echo $timestamp."<br />";
echo $yesterday;*/

/*$var = 5;
function argumentByValue($num){
	$date = date('Y-m-d', strtotime(date('Y-m-d')." -".$num." days"));
	return $date;
	
}*/

/*function argumentByReference(&$num){
	$num +=4;
}
*/
/*function debugger($variable){
	echo "<pre>";
	print_r($variable);
	echo "</pre>";
	exit;
}
*///echo $variable;

/*$array = array('1',2,3,4,5,6,7,8,9,'asdf',array('a','b','d'));
//debugger($array);

$var1 = argumentByValue($var);*/


function debugger($var,$isDie = false){
	echo "<pre>";
	print_r($var);
	echo "</pre>";
	if($isDie){
		exit;
	}
}
?>