<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-26 08:09:58
 * @Organization: Knockout System Pvt. Ltd.
 */

include 'inc/config.php';
include 'inc/generalfunctions.php';

$string = "this is & unsanitized <p>'string'</p>";
$html_string = htmlentities($string);
$string_decode = html_entity_decode($html_string);

$str2 = sanitization($string);


//$sql = "SELECT id,title,status,added_date FROM basic_table WHERE status = true ORDER BY id ASC, added_date DESC LIMIT 1,2";	//Select all the rows form basic_table

//prepared Statements 
$sql = "SELECT id,title,status,added_date FROM basic_table WHERE id='1' ";
$query = $conn->query($sql);		//query the sql 

$count = $query->num_rows;	//No. of rows in $query object

//Fetching Data from database
while ($row = $query->fetch_assoc()) { //mysqli_fetch_assoc()
	debugger($row);
}
exit;
//Insert into database Table
$sql1  = "INSERT INTO basic_table(title,status,added_date) VALUES ('This is another Title.',true,now()),('This is last insert Title.',true,now())"; //Insert SQL Format
$query2 = $conn->query($sql1);	//Running Sql query
$last_insert_id = $conn->insert_id;
echo $last_insert_id;

echo "<br/>";

//Updating Row in table
$sql2 = "UPDATE basic_table SET
		title = 'Updated Title',
		status = false WHERE id = 7 ";
$query2 = $conn->query($sql2);	//Running Update Query
if($query2){
	echo 'table Updated';	
} else {
	die(mysqli_error($conn));
}

//Deleting from table
$sql3 = "DELETE FROM basic_table WHERE id = 10"; //DELETE query
$query3 = $conn->query($sql3);
if($query3){
	echo 'Row id 7 deleted.';	
} else {
	die(mysqli_error($conn));
}
?>