<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-13 07:29:12
 * @Organization: Knockout System Pvt. Ltd.
 */
include 'inc/header.php';


if(!isset($_SESSION['username']) || !isset($_SESSION['role']) || $_SESSION['role']=="" ){
	$_SESSION['error'] = 'You are not logged in.';
	header('location: index.php');
	exit;
}
if($_SESSION['role'] == 'admin'){
?>
	<ul>
		<li><a href="logout.php" title="Logout">Logout</a></li>
		<li><a href="user.php">Users List</a></li>
		<li><a href="post.php">Posts List</a></li>

	</ul>
<?php
} else if($_SESSION['role'] == 'user'){
?>
	<ul>
		<li><a href="post.php">Posts List</a></li>
	</ul>

<?php
} else if($_SESSION['role'] == 'guest'){
?>
	<ul>
		<li><a href="post-detail.php">Posts</a></li>
	</ul>

<?php
}

if($_SESSION['role'] != "admin"){
	$_SESSION['warning'] = "Access Denied";
	header('location: profile.php');
	exit;
}


//Student Information from database
if(isset($_GET['id']) && isset($_GET['action']) && $_GET['action'] == substr(md5('view-user-'.$_GET['id']), 0,10) && $_GET['id'] != ""){
	$id = sanitization($_GET['id']);
	$student = getStudentInfoById($id);
} else {
	$_SESSION['warning'] = "Sorry the page you requested does not exists.";
	header('location: user.php');
}
?>
<div class="container">
	<div class="row">
	<?php 
		include 'inc/notifications.php';
	?>
		<div>
			<h4>Student Information</h4>
			<p>Name: <?php echo $student['name'];?></p>
			<p>Email: <?php echo $student['email_address'];?></p>
			<p>Address: <?php echo $student['address'];?></p>
		</div>
	</div>
</div>

<?php 
include 'inc/footer.php';
?>