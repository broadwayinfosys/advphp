<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Table 2</title>
	<link rel="stylesheet" href="style1.css" />
</head>
<body>
	<h1 align="center">Bus Timing</h1>
	<table cellpadding="2" cellspacing="2" align="center" width="55%" height="55%" border="5">
		<tr>
			<td rowspan="2" colspan="2"></td>
			<td colspan="2"><font color="white"><b>Dharan</b></font></td>
			<td rowspan="2"><font color="white"><b>Biratnagar</b></font></td>
		</tr>

		<tr>
			<td><font color="white"><b>Itahari</b></font></td>
			<td><font color="white"><b>Lahan</b></font></td>
		</tr>
		
		<tr>
			<td colspan="2" bgcolor="red"><font color="white"><b>Week Days</b></font></td>
			<td bgcolor="white">4:35AM</td>
			<td bgcolor="white">6:05AM</td>
			<td bgcolor="white">7:30AM</td>
		</tr>

		<tr>
			<td rowspan="2"><font color="white"><b>Week Ends</b></font></td>
			<td ><font color="white"><b>Sat</b></font></td>
			<td bgcolor="white">5:15AM</td>
			<td bgcolor="white">6:35AM</td>
			<td rowspan="2" bgcolor="white">7:35AM</td>
		</tr>
		<tr>
			<td bgcolor="blue"><font color="white"><b>Sun</b></font></td>
			<td bgcolor="white">--</td>
			<td bgcolor="white">6:30AM</td>
		</tr>
	</table>
</body>
</html>