<!doctype html>
<html>
<head>
	<title>Table 1</title>
	<style type="text/css">
		#body-wrapper {
			background-color: #ccc;
		}
	</style>
</head>

<body class="body-wrapper" id="body-wrapper">
	<h1 align="center">Address Book</h1>
	<table align="center" border="3" cellpadding="2" cellspacing="2" width="50%" height="50%">
		<tr>
			<th>
				Roll No.
			</th>
			<th>
				Name
			</th>
			<th>
				Address
			</th>
			<th>
				Phone Number
			</th>
			<th>
				Email Address
			</th>
		</tr>

		<?php
			$student_info = array(
								array(
									"roll_no" => 1,
									"name" => "Student 1",
									"address" => "Kathmandu",
									"phone_number" => "9802134567",
									"email_address" => "student1@student.com"),
								array("roll_no" => 2,
									"name" => "Student 2",
									"address" => "Lalitpur",
									"phone_number" => "9802132341",
									"email_address" => "student2@student.com"),
								array("roll_no" => 3,
									"name" => "Student 3",
									"address" => "Bhaktapur",
									"phone_number" => "9849132341",
									"email_address" => "student3@student.com"),
								array("roll_no" => 4,
									"name" => "Student 4",
									"address" => "Banepa",
									"phone_number" => "9870132341",
									"email_address" => "student4@student.com"),
								array("roll_no" => 5,
									"name" => "Student 5",
									"address" => "Thankot",
									"phone_number" => "9874132341",
									"email_address" => "student5@student.com")
							);
			foreach($student_info as $key=>$value){ // Array Fetched to populate table row
				echo "<tr>";  // Creates Table row

				foreach($value as $key1=>$value1){
					echo "<td>".$value1."</td>"; // Column of Table created with data population on that column
				}
				echo "</tr>"; // End table row
			}
		?>
	</table>
</body>
</html>