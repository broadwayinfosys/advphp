<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-01-31 07:32:26
 * @Organization: Knockout System Pvt. Ltd.
 */
session_start();
echo "<pre>";
echo "Session Initialized. <br/>";
print_r($_SESSION); 
echo "</pre>";

echo "Session Value Set. <br/>";
$_SESSION['name'] = "Sandesh";
$_SESSION['email'] = "sandesh.bhattarai79@gmail.com";
$_SESSION['userId'] = 1234;

echo "<pre>";
print_r($_SESSION);
echo "</pre>";

/*
echo "Session unset for userId. <br/>";
$_SESSION['userId'] = "";
echo "<pre>";
print_r($_SESSION);
echo "</pre>";

//session_unset($_SESSION['userId']);
echo "<pre>";
print_r($_SESSION);
echo "</pre>";

//session_destroy();
echo "Session Destroy. <br/>";

echo "<pre>";
print_r($_SESSION);
echo "</pre>";*/

?>
