<?php 
	if(isset($_POST['submit']) && $_POST['submit'] =="Submit" && $_POST['action'] == "add"){
		echo "<pre>";
		print_r($_POST);
		echo "</pre>";
		exit;

	} elseif(isset($_POST['action']) && $_POST['action'] != "add"){
		echo "Action undefined.";
		exit;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Contact Form</title>
	<link rel="stylesheet" type="text/css" href="style1.css">
</head>
<body>	
	<form method="post" action="contact-form.php">
		<div class="input-wrap">
			<label>Name: </label>
			<input type="text" name="name" placeholder="Enter your full name" required />
		</div>
		<div class="input-wrap">
			<label>Title: </label>
			<input type="text" name="title" placeholder="Enter title for your message" required />
		</div>
		<div class="input-wrap">
			<label>Subject: </label>
			<input type="text" name="subject" placeholder="Enter subject for Message" required />
		</div>
		<div class="input-wrap">
			<label>Message: </label>
			<textarea name="message" required rows="10" cols="30"></textarea>
		</div>
		<div class="input-wrap">
			<input type="submit" name="submit" class="submit" value="Submit" />
			<input type="hidden" name="action" value="add" />
		</div>

	</form>
</body>
</html>