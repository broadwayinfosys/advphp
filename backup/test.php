<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-01-27 08:23:38
 * @Organization: Knockout System Pvt. Ltd.
 */
// -1 , 0, 1
//-1 : Displays all errors, warning, stricts, notices, depricated values
// 1 : Stricts, errors 
// 0 : Displays nothing
// If Environment: Development
error_reporting(-1);

// If Environment: Production
error_reporting(0);
ini_set();
mysqli_connect();

// If Environment: Staging
error_reporting(1);


?>
