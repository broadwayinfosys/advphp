<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Home page</title>
	<link rel="stylesheet" href="style.css">
</head>
<body bgcolor="grey">
	<h1 align="center">Bus Timing</h1>
	<table align="center" border="3" cellpadding="2" cellspacing="2" height="50%" width="50%" style="display: none;">
		<tr>
			<th>Roll No.</th>
			<th>Name</th>
			<th>Address</th>
			<th>Phone Number</th>
			<th>Email Address</th>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	</table>


	<div class="table2" style="display:block;">
		<table align="center" border="5" cellpadding="2" cellspacing="2" height="55%" width="55%" bordercolor="0000ff">
			<tr bgcolor="blue" align="center">
				<td rowspan="2" colspan="2"></td>
				<td colspan="2" align="center">
					<b>
						<font color="white">Dharan</font>
					</b>
				</td>
				<td align="center" rowspan="2">
					<b>
						<font color="white">Biratnagar</font>
					</b>
				</td>
			</tr>
			
			<tr bgcolor="blue" align="center">
				<td><b><font color="white">Itahari</font></b></td>
				<td><b><font color="white">Lahan</font></b></td>
			</tr>

			<tr bgcolor="red" align="center">
				<td colspan="2"> <font color="white"><b>Week Days</b></font></td>
				<td bgcolor="white">4:35AM</td>
				<td bgcolor="white">6:05AM</td>
				<td bgcolor="white">7:30AM</td>
			</tr>

			<tr bgcolor="blue" align="center">
				<td rowspan="2"><font color="white"><b>Week Ends</b></font></td>
				<td><font color="white"><b>Sat</b></font></td>
				<td bgcolor="white">5:15AM</td>
				<td bgcolor="white">6:35AM</td>
				<td bgcolor="white" rowspan="2">7:35AM</td>
			</tr>

			<tr bgcolor="red" align="center">
				<td bgcolor="blue"><font color="white"><b>Sun</b></font></td>
				<td bgcolor="white">--</td>
				<td bgcolor="white">6:30AM</td>
				
			</tr>
		</table>
	</div>
</body>
</html>