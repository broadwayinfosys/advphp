<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-13 07:29:12
 * @Organization: Knockout System Pvt. Ltd.
 */
include 'inc/header.php';

if(!isset($_SESSION['username']) || !isset($_SESSION['role']) || $_SESSION['role']=="" ){
	$_SESSION['error'] = 'You are not logged in.';
	header('location: index.php');
	exit;
}
if($_SESSION['role'] == 'admin'){
?>
	<ul>
		<li><a href="logout.php" title="Logout">Logout</a></li>
		<li><a href="user.php">Users List</a></li>
		<li><a href="post.php">Posts List</a></li>

	</ul>
<?php
} else if($_SESSION['role'] == 'user'){
?>
	<ul>
		<li><a href="post.php">Posts List</a></li>
	</ul>

<?php
} else if($_SESSION['role'] == 'guest'){
?>
	<ul>
		<li><a href="post-detail.php">Posts</a></li>
	</ul>

<?php
}

if($_SESSION['role'] != "admin"){
	$_SESSION['warning'] = "Access Denied";
	header('location: profile.php');
	exit;
}


//To delete user
if(isset($_GET['id']) && isset($_GET['action']) && $_GET['action'] == substr(md5('delete-user-'.$_GET['id']), 0,10) && $_GET['id'] != ""){
	$id = sanitization($_GET['id']);
	$student = getStudentInfoById($id);
	
	if($student){	//if Student informaiton exists in database
		$del = deleteData("student_info", "id", $id); //Delete the data 
		if($del){
			$_SESSION['success'] = "Student information deleted successfully";
			header('location: user.php');
			exit;
		} else {
			$_SESSION['error'] = "There was problem while deleting student information.";
			header('location: user.php');
			exit;
		}
	} else {
		$_SESSION['warning'] = "Sorry! The student information does not exists.";
		header('location: user.php');
		exit;
	}
}
?>
<div class="container">
	<div class="row">
	<?php 
		include 'inc/notifications.php';
	?>
	<div class="col-md-10">
		<h4>Student Lists</h4>
		<table class="table table-responsive table-hover table-bordered">
			<thead>
				<th>S.N.</th>
				<th>Name</th>
				<th>Email</th>
				<th>Address</th>
				<th>Phone Number</th>
				<th>Action</th>
			</thead>
			<tbody>
				<?php
					$student_info = getAllStudents();
					if($student_info){
						$i = 1;
						foreach($student_info as $students){
							?>
							<tr>
								<td><?php echo $i;?></td>
								<td>
									<?php 
										echo $students['name'];
									?>
								</td>
								<td>
								<?php 
									echo $students['email_address'];
								?>
								</td>
								<td>
								<?php 
									echo $students['address'];
								?>
								</td>
								<td>
								<?php 
									echo $students['phone_number'];
								?>
								</td>
								<td>
									<a href="user-add.php?id=<?php echo $students['id'];?>&action=<?php echo substr(md5('edit-user-'.$students['id']),0,10);?>">
										<i class="fa fa-pencil"></i> Edit
									</a>
									<a href="user-view.php?id=<?php echo $students['id'];?>&action=<?php echo substr(md5('view-user-'.$students['id']),0,10);?>">
										<i class="fa fa-eye"></i> View
									</a>
									<a href="user.php?id=<?php echo $students['id'];?>&action=<?php echo substr(md5('delete-user-'.$students['id']),0,10);?>">
										<i class="fa fa-trash"></i> Delete
									</a>
								</td>
							</tr>
							<?php
							$i++;
						}
					} else {
						echo "<tr><td colspan='6'>There are no any student information in the table.</td></tr>";
					}
				?>
			</tbody>
		</table>
		<a href="user-add.php">Student Add</a>
	</div>
	</div>
</div>

<?php 
include 'inc/footer.php';
?>