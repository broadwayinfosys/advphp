<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-19 07:28:27
 * @Organization: Knockout System Pvt. Ltd.
 */
include 'inc/functions.php';
$path = "files";

if(!file_exists($path) && !is_dir($path)){
	mkdir($path);
}
$filename = $path."/test.json";
$textFile = $path."/test.txt";

/*$file = fopen($path.'/test.txt','w') or die('Cannot open file.');
fclose($file);
*/
//File open modes: Write: 1) w 2) a, Read: 1) r

/*file_put_contents($path."/test2.txt", "");

echo "here is another line of code.";*/

//How to read from file
/*$file = fopen($path."/test.txt", 'r') or die('Cannot open file.');
$data = fread($file, filesize($path."/test.txt"));
fclose($file);*/

//File Write Content 

/*$file = fopen($filename, 'a') or die('cannot open file');
$data = "This is another file content";
fwrite($file, $data);
fclose($file);*/
/*
$readPointer = fopen($filename, 'r');
$data1 = fread($readPointer, filesize($filename));
fclose($readPointer);*/
$data1 = file_get_contents($filename);

$data = $data1."This is another file content";
file_put_contents($filename, $data);

$data1 = file_get_contents($filename);



$array = array(
		array(
			"name"=>"Sandesh Bhattarai",
			"address"=>"Kadaghari, Kathmandu",
			"email"=>"sandesh.bhattarai79@gmail.com",
			"phone"=>9802111635),
		array(
			"name"=>"Prasun Jajodia",
			"address"=>"Bhimsengola, Kathmandu",
			"email"=>"prasunjajodia@gmail.com",
			"phone"=>1234567890),
		array(
			"name"=>"Deepak Paudel",
			"address"=>"Samakhusi, Kathmandu",
			"email"=>"deepak@knockoutsystem.com",
			"phone"=>2134567890),
		array(
			"name"=>"Prabhat Man Shrestha",
			"address"=>"Dhalku, Kathmandu",
			"email"=>"prabhatmanshrestha@gmail.com",
			"phone"=>9803215564)
		);

$data1 = serialize($array);
//echo $data1;


$data1 = json_encode($array);
//echo $data1;
file_put_contents($filename, $data1);

$json_data = file_get_contents($filename);
$data = json_decode($json_data); //Returns Std Class Objects

/*echo $array[0]['name'];

echo $data[0]->name;
*/
echo "<h1>Personal Information</h1>";
foreach ($data as $value) {
	echo "Name: ".$value->name."<br />";
	echo "Address: ".$value->address."<br />";
	echo "Email: ".$value->email."<br />";
	echo "Phone Number: ".$value->phone."<br />"."<br />";
}


$file = fopen($textFile, 'r') or die("Cannot open the file.");
//$data = fgetc($file);

while(!feof($file)){
	echo fgetc($file);
}
debugger($data,true);


?>
